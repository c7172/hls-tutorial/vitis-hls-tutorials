# Configuration
set project_name        complex_pow2
set project_part        xc7z010clg400-1; #zybo
set name_top_function   ${project_name}
set vendor              citrobits_gmbh

# Create project
open_project -reset ${project_name}.prj

# Set project name as top function
set_top $name_top_function

# Add files
add_files ./src/cpp/${project_name}.cpp
add_files -tb ./src/cpp/${project_name}_tb.cpp

# Solutions
    set all_solutions [list solution1]
    foreach solution $all_solutions {
        # Create solution
        open_solution -reset "$solution"
        set_part $project_part
        create_clock -period 10 -name default

        # Apply directives
        source "./src/directives/$solution/directives.tcl"

        # Execute csim
        csim_design

        # Execute csynth
        csynth_design

        # Execute cosim
        cosim_design

        # Export customized ip
        puts "Exporting ${vendor}_IP_${project_name}_hw_${solution} to local project: ./${project_name}.prj/${solution}/impl/ip"
        export_design -format ip_catalog -vendor "${vendor}" -version "1.0.0"
    }
    exit
