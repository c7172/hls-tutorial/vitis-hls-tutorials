#include "complex_pow2.h"

void sw_complex_pow2(int re_in, int im_in, int *re_out, int *im_out) {
    *re_out = re_in * re_in - im_in * im_in;
    *im_out = 2 * re_in * im_in;
}

int main() {
    /* Calculate golden output values using the software function */
    int sw_re_in, sw_im_in, sw_re_out, sw_im_out;
    sw_re_in = 2;
    sw_im_in = 1;
    sw_complex_pow2(sw_re_in, sw_im_in, &sw_re_out, &sw_im_out);

    /* Calculate hardware output values using the hardware function */
    int hw_re_in, hw_im_in, hw_re_out, hw_im_out;
    hw_re_in = 2;
    hw_im_in = 1;
    complex_pow2(hw_re_in, hw_im_in, &hw_re_out, &hw_im_out);

    /* Compare golden and hardware values */
    if (hw_re_out != sw_re_out || hw_im_out != sw_im_out)
        return -1;

    return 0;
}