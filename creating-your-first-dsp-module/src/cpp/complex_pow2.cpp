#include <stdio.h>

/* The complex_pow2 function takes a real and imaginary part of a complex number
 * and performs the following operation:
 * (a + bi)^2 = (a^2 - b^2) + (2ab)i
 */

void complex_pow2(int REin, int IMin, int *REout, int *IMout) {
    *REout = REin * REin - IMin * IMin;
    *IMout = 2 * REin * IMin;
}
